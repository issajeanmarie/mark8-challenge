# Frontend Developer Challenge

Welcome to the Frontend Developer Challenge! We are excited to see your skills and creativity in action. Below are the details and requirements for the challenge. Please read through carefully and adhere to the guidelines provided.

## Challenge Overview

You will be building a project using **Next.js** and **TypeScript**. The primary UI library we recommend is **AntDesign**, but feel free to use any other library if you prefer. The challenge aims to evaluate your proficiency in frontend development, code organization, and optional bonus tasks.

### Project Description

You will be developing the client side of an e-commerce system where users can view and purchase products, and store owners can create stores and list their products. The frontend should look exactly like the provided UI designs and be 100% responsive.

### UI Designs

You can find the design mockups [here](https://www.figma.com/design/zwuXM6IMryYZjjDvztYDyd/Mark8?node-id=0-1&t=D2ZSPJ6KWQBaJin0-1).

## Requirements

### Mandatory

1. **Framework**: Next.js
2. **Language**: TypeScript
3. **UI Library**: AntDesign (or any other of your choice)
4. **Folder Structure**: Follow a standard folder structure and code organization practices.
5. **ReadMe File**: Provide a concise README file with instructions on how to install and run the application.

### Optional (Bonus Points)

1. **Tests**: Writing tests for your application.
2. **CI/CD Pipelines**: Setting up Continuous Integration and Continuous Deployment pipelines.
3. **Docker**: Dockerizing your application.

### API Details

You can find the API endpoints [here](http://api.mark8.awesomity.rw/api-docs). These endpoints provide all the necessary data for the project. If an endpoint is not provided, focus on building the UI only.

*NB: For more details on how to use API and the designs please read this document: [here](https://gitlab.com/issajeanmarie/mark8-challenge/-/blob/main/API-AND-DESIGNS-README.md?ref_type=heads).*

## Git Configuration

To ensure unbiased judgment, please change your Git configuration as follows:

1. **Change Git Username**:
    ```bash
    git config --global user.name "mark8-[a random number between 0-100]"
    ```

2. **Change Git Email**:
    ```bash
    git config --global user.email "secret@awesomity.rw"
    ```

3. **No Personal Details**: Do not include any personal details in the codebase or documentation.

## Project Timeline

The challenge should be completed within 2 weeks. However, submitting your work before the deadline will increase your chances.

## Submission

Send an email with a link to your repository to [issajeanmarie@awesomity.rw](mailto:issajeanmarie@awesomity.rw) and CC [iradukundaolivier@awesomity.rw](mailto:iradukundaolivier@awesomity.rw), [kabayizayannick@awesomity.rw](mailto:kabayizayannick@awesomity.rw), and [briangitego@awesomity.rw](mailto:briangitego@awesomity.rw). It would be advantageous if you deploy your project using free services such as Vercel or Netlify and include the deployment link in your submission.

We look forward to seeing your work. Good luck and happy coding!

---

Please let us know if you have any questions or need further clarification on any part of the challenge.
